
#include <bits/stdc++.h>
using namespace std;

bool dfs(vector<char>* neighbours, bool* processed, char start, int& trees, bool top_level = true) {
	if (processed[start]) return true;
	processed[start] = true;

	bool non_empty = false;
	for (auto const& n: neighbours[start]) {
		non_empty = true;
		dfs(neighbours, processed, n, trees, false);
	}

	if (top_level) trees++;
	return non_empty;
}

int main() {
	char ch;
	string asdf;
	int n_cases;
	cin >> n_cases;
	getline(cin, asdf);
	
	for (int c = 0; c < n_cases; c++) {
		vector<char> neighbours[26];
		bool processed[26];
		bool has_n[26];

		for (int i = 0; i < 26; i++) {
			has_n[i] = false;
			processed[i] = false;
			neighbours[i] = vector<char>();
		}

		while (1) {
			string ln = "";
			getline(cin, ln);

			if (ln[0] == '*') break;

			char from = ln[1] - 65;
			char to = ln[3] - 65;

			//cout << "  got " << (char)(from + 65) << ", " << (char)(to + 65) << endl;

			neighbours[from].push_back(to);
			neighbours[to].push_back(from);
			has_n[from] = true;
			has_n[to] = true;
		}

		int trees = 0;
		int acorns = 0;
		while (!cin.eof() && cin.peek() != '(' && cin.peek() != '*') {
			char node;
			cin >> node;

			//cout << "dfs on " << node << endl;

			if (!dfs(neighbours, processed, node - 65, trees)) {
				acorns++;
			}

			//cout << "processed " << node << endl;

			if (cin.eof() || cin.peek() != ',') break;
			cin >> ch;
		}

		cout << "There are " << trees - acorns << " tree(s) and " << acorns << " acorn(s)." << endl;
		getline(cin, asdf);
	}

	return 0;
}
